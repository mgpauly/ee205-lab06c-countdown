///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Maxwell Pauly <mgpauly@hawaii.edu>
// @date   21_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#include <time.h>
#include <stdio.h>

int main() {
   time_t rawtime;
   time (&rawtime);
   struct tm* currentTime = localtime(&rawtime);

   // Counting down to Friday, January 10, 1970, 0hours, 0min, 0sec, the date the currentTime initiates away from
   while (!subtractSecond(currentTime)) {
   printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d\n", currentTime->tm_yday, currentTime->tm_yday, currentTime->tm_hour, currentTime->tm_min, currentTime->tm_sec);
   sleep(1);
   }
   return 0;
}

int subtractYear(struct tm* time) {
   if (time->tm_year == 0) {
      printf("Happy New Year!!!");
      return 1;
   }
   time->tm_year--;
   return 0;
}

int subtractDay(struct tm* time) {
   if (time->tm_yday == 0) {
      time->tm_yday = 365;
      return subtractYear(time);
   }
   time->tm_yday--;
   return 0;
}

int subtractHour(struct tm* time) {
   if (time->tm_hour == 0) {
      time->tm_hour = 23;
      return subtractDay(time);
   }
   time->tm_hour--;
   return 0;
}

int subtractMinute(struct tm* time) {
      if (time->tm_min == 0) {
      time->tm_min = 59;
      return subtractHour(time);
   }
   time->tm_min--;
   return 0;
}

int subtractSecond(struct tm* time) {
   // printf("seconds = %d\n", time->tm_sec);
   if (time->tm_sec == 0) {
      time->tm_sec = 59;
      return subtractMinute(time);
      }
   time->tm_sec--;
   return 0;
   // printf("seconds = %d\n", time->tm_sec);
}
